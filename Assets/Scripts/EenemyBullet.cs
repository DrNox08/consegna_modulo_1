using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EenemyBullet : MonoBehaviour
{
    [SerializeField]
    private float speed;
   
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }
}
