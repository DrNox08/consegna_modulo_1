using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public delegate void Boss();
    
    public static event Boss OnBoss;
    [SerializeField] private float camSpeed; // default is 2
    
    private void OnEnable()
    {
        SectionsTrigger.OnThirdSection += Stop;
        GameManager.OnStartingApplication += Stop;
        GameManager.OnStartFirstSection += Go;
        GameManager.OnStartSecondSection += Go;
        GameManager.OnStartThirdSection += Go;
        PlayerController.OnSpeeder += SpeedUpOnThirdSection;
        GameManager.Section3Cleared += Go;
    }

    private void OnDisable()
    {
        SectionsTrigger.OnThirdSection -= Stop;
        GameManager.OnStartingApplication -= Stop;
        GameManager.OnStartFirstSection -= Go;
        GameManager.OnStartSecondSection -= Go;
        GameManager.OnStartThirdSection -= Go;
        PlayerController.OnSpeeder -= SpeedUpOnThirdSection;
    }

    
    void Update()
    {
        transform.Translate(Vector3.right * camSpeed * Time.deltaTime);

        if (transform.position.x >= 212.48 && transform.position.x <= 215)
        {
            OnBoss?.Invoke();
            Stop();
        }
        
    }

    void Stop()
    {
        camSpeed = 0f;
    }

    void Go()
    {
        camSpeed = 1.5f;
    }

    void SpeedUpOnThirdSection()
    {
        StartCoroutine(SpeedUp());

    }

    IEnumerator SpeedUp()
    {
        while (transform.position.x < 160)
        {
            camSpeed = 30;
            yield return null;
        }

        camSpeed = 2;
        
            
    }
}
