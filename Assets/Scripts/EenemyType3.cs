using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class EenemyType3 : MonoBehaviour
{
    public delegate void EnemyDeath();
    public static event EnemyDeath OnEnemyDead;
    [SerializeField] GameObject baseBullet;
    public float speed = 15f; 
    private Vector2[] directions = new Vector2[] { Vector2.up, Vector2.down, Vector2.left, Vector2.right }; 
    private float minY = -7f; 
    private float maxY = 7f;
    private float maxX;
    private float minX;
    int hp = 100;
    float spawnTime;
    float biasDuration = 5f;

    void Start()
    {
        spawnTime = Time.time;
        maxX = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0 )).x;
        minX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).x;
        StartCoroutine(RandomMove());
        StartCoroutine(ShootOnCardinals());
    }

     void Update()
    {
        if (hp <= 0) { OnEnemyDead?.Invoke(); Destroy(gameObject); }
    }

    IEnumerator RandomMove()
    {
        while (true) 
        {
            Vector2 randomDirection = directions[Random.Range(0, directions.Length)];
            float randomDelay = Random.Range(1f, 4f);
            float randomDistance = Random.Range(4f, 15f);
            float startTime = Time.time;

            if (startTime - spawnTime <= biasDuration)
            {
                randomDirection = BiasOnGoingLeft(randomDirection);
            }

            Vector2 newPosition = (Vector2)transform.position + (randomDirection * randomDistance);

            
            if (newPosition.y > maxY)
            {
                newPosition.y = maxY;
                randomDirection.y *= -1;
            }
                
            else if (newPosition.y < minY)
            {
                newPosition.y = minY;
                randomDirection.y *= -1;
            }
                
            if (newPosition.x > maxX)
            {
                newPosition.x = maxX;
                randomDirection.x *= -1;
            }

            if (newPosition.x < minX)
            {
                newPosition.x = minX;
                randomDirection.x *= -1;
            }

            float moveTime = randomDistance / speed;

            while (Time.time < startTime + moveTime)
            {
                transform.position = Vector2.MoveTowards(transform.position, newPosition, speed * Time.deltaTime);
                yield return null;
            }

            yield return new WaitForSeconds(randomDelay);
        }
    }

    Vector2 BiasOnGoingLeft(Vector2 currentDirection)
    {
        
        if (Time.time - spawnTime <= biasDuration)
        {
            return Vector2.left;
        }
        else 
        {
            return directions[Random.Range(0, directions.Length)];
        }
    }


    IEnumerator ShootOnCardinals()
    {
        while (true)
        {
            Instantiate(baseBullet, transform.position, Quaternion.identity);
            Instantiate(baseBullet, transform.position, Quaternion.Euler(0, 0, 90));
            Instantiate(baseBullet, transform.position, Quaternion.Euler(0, 0, 180));
            Instantiate(baseBullet, transform.position, Quaternion.Euler(0, 0, 270));
            yield return new WaitForSeconds(1);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerBullet"))
        {
            hp--;
        }
    }

    private void OnDestroy()
    {
        GameManager.ThirdSectionCounter++;
    }






}
