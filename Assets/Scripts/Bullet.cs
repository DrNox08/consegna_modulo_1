using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    
    public float bulletSpeed = 1;
    SpriteRenderer spriteRenderer;
    [SerializeField] Sprite explosion;
    Collider2D coll;
    bool moving = true;



    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        coll = GetComponent<Collider2D>();


    }

    // Update is called once per frame
    void Update()
    {
        if (moving) { transform.Translate(Vector2.right * bulletSpeed * Time.deltaTime); }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            moving = false;
            coll.enabled = false;
            transform.localScale = new Vector3(5,5,5);
            spriteRenderer.sprite = explosion;
            Destroy(gameObject,0.2f);
        }

        if (collision.gameObject.CompareTag("Boss"))
        {
            moving = false;
            coll.enabled = false;
            transform.localScale = new Vector3(5, 5, 5);
            spriteRenderer.sprite = explosion;
            Destroy(gameObject, 0.2f);
        }



    }



}
