using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectionsTrigger : MonoBehaviour
{
    public delegate void Sections();
    public static event Sections OnFirstSection;
    public static event Sections OnSecondSection;
    public static event Sections OnThirdSection;
    public static event Sections OnBossSection;

    

    Vector2 firstTrigger;
    Vector2 secondTrigger;
    Vector2 thirdTrigger;
    Vector2 bossTrigger;
   

    

    private void Start()
    {
        firstTrigger = transform.position;
        secondTrigger = new Vector2(81, 0);
        thirdTrigger = new Vector2(179,0);
        bossTrigger = new Vector2(196,0);

    }


    private void OnEnable()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && transform.position.x == firstTrigger.x)
        {
            transform.position = secondTrigger;
            OnFirstSection?.Invoke();
        }

        else if (collision.gameObject.CompareTag("Player") && transform.position.x == secondTrigger.x)
        {
            transform.position = thirdTrigger;
            OnSecondSection?.Invoke();
        }

        else if (collision.gameObject.CompareTag("Player") && transform.position.x == thirdTrigger.x)
        {
            transform.position = bossTrigger;
            OnThirdSection?.Invoke();
        }

        else if (collision.gameObject.CompareTag("Player") && transform.position.x == bossTrigger.x)
        {
            transform.position = bossTrigger;
            OnBossSection?.Invoke();
            Destroy(gameObject);
        }

    }
}
