using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public delegate void GetDamage();
    public static event GetDamage OnGetHit;
    public static event GetDamage OnHealing;
    public static event GetDamage OnPlayerDeath;
    public delegate void OnColliders();
    public static event OnColliders OnSpeeder;

    [SerializeField] GameObject supportUp;
    [SerializeField] GameObject supportDown;
    float moveX;
    float moveY;
    public GameObject bullet;
    Camera cam;
    float xMin, yMin;
    float xMax, yMax;
    [Header("Player")]
    public float moveSpeed;
    public float fireRate;
    float fireTime;
    public float padding;


    public int hp = 4;
    public bool powerUpActive;
    public bool powerUpPlus;


    private void OnEnable()
    {
        OnGetHit += LoseHp;
    }

    private void OnDisable()
    {
        OnGetHit -= LoseHp;
    }




    void Start()
    {
        cam = Camera.main;
        supportUp.SetActive(false);
        supportDown.SetActive(false);
        powerUpActive = false;
        
    }


    void Update()
    {
        
        if (hp <= 0) { Death(); }
        moveX = Input.GetAxis("Horizontal");
        moveY = Input.GetAxis("Vertical");

        Bounds();
        float newXPos = Mathf.Clamp(transform.position.x + moveX * moveSpeed * Time.deltaTime, xMin, xMax);
        float newYPos = Mathf.Clamp(transform.position.y + moveY * moveSpeed * Time.deltaTime, yMin, yMax);
        transform.position = new Vector2(newXPos, newYPos);

        if (Input.GetKey(KeyCode.Space) && Time.time >= fireTime && powerUpActive == false) { Shoot(); }
        if (Input.GetKey(KeyCode.Space) && Time.time >= fireTime && powerUpActive == true) { PowerShoot(); }
        if (Input.GetKey(KeyCode.Space) && Time.time >= fireTime && powerUpPlus == true) { PowerShoot(); }
        
    }
    void Shoot()
    {
            fireTime = Time.time + fireRate;
            Instantiate(bullet, transform.position + transform.right, Quaternion.identity);
    }
        
        
        
        

    void PowerShoot()
    {
        fireTime = Time.time + fireRate;
        Instantiate(bullet, transform.position + transform.right, Quaternion.identity);
        Instantiate(bullet, supportUp.transform.position, Quaternion.Euler(0, 0, 25));
        Instantiate(bullet, supportDown.transform.position, Quaternion.Euler(0,0,-25));
        
    }
    IEnumerator PowerUpTimer(float timer)
    {
        yield return new WaitForSecondsRealtime(timer);
        powerUpActive = false;
        powerUpPlus = false;
        supportUp.SetActive(false);
        supportDown.SetActive(false);

    }

    void Bounds()
    {
        xMin = cam.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding + 0.25f;
        xMax = cam.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding - 0.25f;

        yMin = cam.ViewportToWorldPoint(new Vector3(0, -0.01f, 0)).y + padding;
        yMax = cam.ViewportToWorldPoint(new Vector3(0, 1.02f, 0)).y - padding;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            OnGetHit();
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("Enemy"))
        {
            OnGetHit();
        }

        if (collision.gameObject.CompareTag("Healer"))
        {
            hp = 5;
            OnHealing?.Invoke();
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("PowerUp"))
        {
            powerUpActive = true;
            supportUp.SetActive(true);
            supportDown.SetActive(true);
            Destroy(collision.gameObject);
            StartCoroutine(PowerUpTimer(15));
        }

        if (collision.gameObject.CompareTag("Speeder"))
        {
            Destroy(collision.gameObject);
            OnSpeeder?.Invoke();
        }

        if (collision.gameObject.CompareTag("PowerUpPlus"))
        {

            powerUpActive = true;
            supportUp.SetActive(true);
            supportDown.SetActive(true);
            Destroy(collision.gameObject);
            StartCoroutine(PowerUpTimer(30));
        }

        if (collision.gameObject.CompareTag("Boss"))
        {
            OnGetHit();
        }
    }

    void LoseHp()
    {
        hp--;
    }

    void Death()
    {
        OnPlayerDeath?.Invoke();
        Destroy(gameObject);
    }

    

    





















}
