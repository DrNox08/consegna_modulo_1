using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance = null;
    [SerializeField] Image hpBar;
    [SerializeField] TMP_Text killCount;
    [SerializeField] RectTransform startMenu;
    int kills;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        
    }
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        PlayerController.OnGetHit +=() => hpBar.fillAmount -= 0.20f;
        EenemyType1.OnEnemyDead += () => kills++;
        EnemyType2.OnEnemyDead += () => kills++;
        EenemyType3.OnEnemyDead += () => kills++;
        PlayerController.OnHealing += () => hpBar.fillAmount = 1;

        GameManager.OnStartFirstSection += () => startMenu.anchoredPosition = Vector2.up * 1000;
        GameManager.OnStartSecondSection += () => startMenu.anchoredPosition = Vector2.up * 1000;
        GameManager.OnStartThirdSection += () => startMenu.anchoredPosition = Vector2.up * 1000;
        
    }

    private void OnDisable()
    {
        PlayerController.OnGetHit -= () => hpBar.fillAmount -= 0.20f;
        EenemyType1.OnEnemyDead -= () => kills++;
        EnemyType2.OnEnemyDead -= () => kills++;
        EenemyType3.OnEnemyDead -= () => kills++;
        PlayerController.OnHealing -= () => hpBar.fillAmount = 1;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        killCount.text = "Kills: " + kills.ToString();
        
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        hpBar.fillAmount = 1;
        startMenu.anchoredPosition = Vector2.zero;
        kills = 0;
    }

    




}
