using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyType2 : MonoBehaviour
{
    public delegate void EnemyType2Death();
    public static event EnemyType2Death OnEnemyDead;
    [SerializeField] GameObject baseBullet;
    //float fireRate = 0.6f;
    int hp = 5;
    public float speed = 5f;

    Vector2 pos;
    public float amplitude = 1f;
    public float frequency = 2f;
    public float yOffset;


    void Start()
    {
        pos = transform.position;
        yOffset = Random.Range(0f, 4f);
        Shoot();
    }

    
    void Update()
    {
        //horizontal
        transform.Translate(Vector2.left * speed * Time.deltaTime);
        //vertical
        float verticalMovement = Mathf.Sin((Time.time + yOffset) * frequency) * amplitude;
        transform.position = new Vector3(transform.position.x, pos.y + verticalMovement, transform.position.z);

        if (hp <= 0) { OnEnemyDead?.Invoke(); Destroy(this.gameObject); }
    }

    void Shoot()
    {
        StartCoroutine(AsyncShoot());
    }


    IEnumerator AsyncShoot() 
    {
        while (true)
        {
            Instantiate(baseBullet, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(1);
            Instantiate(baseBullet, transform.position, Quaternion.Euler(0,0,30));
            yield return new WaitForSeconds(1);
            Instantiate(baseBullet, transform.position , Quaternion.Euler(0,0,-30));
            yield return new WaitForSeconds(1);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerBullet"))
        {
            hp--;
        }
    }
}
