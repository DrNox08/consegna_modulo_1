
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EenemyType1 : MonoBehaviour
{
    public delegate void EenemyType1Dead();
    public static event EenemyType1Dead OnEnemyDead;
    [SerializeField] GameObject baseEnemyBullet;
    [SerializeField] float fireRate = 0.5f;
    [SerializeField]float speed;
    
    public int hp = 2;
    Vector2 pos;
    float ampli = 1f;
    float frequency = 1f;
    float yOffset;

    


    void Start()
    {
        pos = transform.position;
        yOffset = Random.Range(0f, 2f);
        InvokeRepeating("Shoot", 1f, fireRate);
    }

    // Update is called once per frame
    void Update()
    {
        //horizontal
        transform.Translate(Vector2.left * speed*Time.deltaTime);
        //vertical
        float verticalMovement = Mathf.Sin((Time.time + yOffset) * frequency) * ampli;
        transform.position = new Vector3(transform.position.x, pos.y + verticalMovement, transform.position.z);



        if (hp <= 0) { OnEnemyDead?.Invoke(); Destroy(this.gameObject); }
    }

    void Shoot()
    {
        Instantiate(baseEnemyBullet, transform.position, Quaternion.identity);
    }

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerBullet"))
        {
            hp--;
        }
    }


}
