using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shredders : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("Projectile") || collision.gameObject.CompareTag("PlayerBullet"))
        {
            
            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile") || collision.gameObject.CompareTag("PlayerBullet") || collision.gameObject.CompareTag("Enemy"))
        {

            Destroy(collision.gameObject);
        }
    }
}
