using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    private float speed = 0.1f;
    public Renderer background;

    private void OnEnable()
    {
        GameManager.OnStartingApplication += Stop;
        PlayerController.OnSpeeder += SpeedOnThirdSection;
        CameraScript.OnBoss += Stop;

    }

    private void OnDisable()
    {
        GameManager.OnStartingApplication -= Stop;
        PlayerController.OnSpeeder -= SpeedOnThirdSection;
        CameraScript.OnBoss -= Stop;
    }


    void Update()
    {
        background.material.mainTextureOffset += new Vector2(speed * Time.deltaTime, 0);
    }

    void Stop()
    {
        speed = 0;
    }

    void Go()
    {
        speed = 2;
    }

    void SpeedOnThirdSection()
    {
        StartCoroutine(SpeedStop());
    }

    IEnumerator SpeedStop() 
    {
        speed = 3;
        yield return new WaitForSeconds(2);
        speed = 0.1f;

    }
}
