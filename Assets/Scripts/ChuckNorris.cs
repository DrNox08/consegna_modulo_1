using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChuckNorris : MonoBehaviour
{
    public delegate void EndGame();
    public static event EndGame OnEndGame;

    [SerializeField] Transform leftShooter;
    [SerializeField] Transform rightShooter;
    [SerializeField] GameObject baseBullet;
    Collider2D[] allColliders;
    

    int hp = 1000;

    private void OnEnable()
    {
        SectionsTrigger.OnBossSection += Shoot;
        SectionsTrigger.OnBossSection += CollidersOn;
    }

    private void OnDisable()
    {
        SectionsTrigger.OnBossSection -= Shoot;
        SectionsTrigger.OnBossSection -= CollidersOn;
    }



    private void Start()
    {
        allColliders = GetComponents<Collider2D>();
        foreach (Collider2D col in allColliders) { col.enabled = false; }
    }

    private void Update()
    {
        if (hp <= 0) { OnEndGame?.Invoke();  }
    }

    void Shoot()
    {
        StartCoroutine(AsyncShoot());
    }


    IEnumerator AsyncShoot()
    {
        while (true)
        {
            
            //int randomDelay = Random.Range(1, 2);
            float randomAngle = Random.Range(-85f, 85f);
            Quaternion bulletRotation = Quaternion.Euler(0, 0, randomAngle);
            Instantiate(baseBullet, leftShooter.position, bulletRotation);
            yield return new WaitForSeconds(0.3f); 
            randomAngle = Random.Range(-85, 85f);
            bulletRotation = Quaternion.Euler(0, 0, randomAngle);
            Instantiate(baseBullet, rightShooter.position, bulletRotation);
            yield return new WaitForSeconds(0.5f); 

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerBullet"))
        {
            hp--;
            
        }
    }

    void CollidersOn()
    {
        foreach (Collider2D col in allColliders) { col.enabled = true; }
    }

    
    







}
