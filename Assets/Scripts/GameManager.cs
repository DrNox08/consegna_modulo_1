using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public delegate void GameState();
    public static event GameState OnStartingApplication;
    public static event GameState OnStartFirstSection;
    public static event GameState OnStartSecondSection;
    public static event GameState OnStartThirdSection;
    public static event GameState Section3Cleared;
    public static event GameState OnPause;


    GameObject player;
    GameObject sectionTrigger;
    GameObject cam;
    Transform boss;
    RectTransform endScreenMeme;
    RectTransform endScreen;

    public static int ThirdSectionCounter;
    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        
        




    }

    private void Start()
    {
       
        

        
    }
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        //SectionsTrigger.onBossSection += () => boss.transform.position = ;
        ChuckNorris.OnEndGame += EndScreenFinal;
        PlayerController.OnPlayerDeath += EndScreenDeath;
        
        
    }

    private void OnDisable()
    {
        ChuckNorris.OnEndGame -= EndScreenFinal;
        PlayerController.OnPlayerDeath -= EndScreenDeath;
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }



    private void Update()
    {
        if ( ThirdSectionCounter >= 5)
        {
            Section3Cleared?.Invoke();
            ThirdSectionCounter = 0;
        }

        if (Input.GetKeyDown(KeyCode.Escape) && Time.timeScale >= 1) { Pause(); }
        else if (Input.GetKeyDown(KeyCode.Escape) && Time.timeScale <= 1) { Time.timeScale = 1; }
    }

    public void StartFirstSection()
    {
        Time.timeScale = 1f;
       // player.SetActive(true);
        OnStartFirstSection?.Invoke();
    }

    public void StartSecondSection() 
    {
        Time.timeScale = 1f;
        cam.transform.position = new Vector3(75, 0, -1);
        //player.SetActive(true);
        sectionTrigger.transform.position = new Vector2(81, 0);
        OnStartSecondSection?.Invoke();
    }

    public void StartThirdSection() 
    {
        Time.timeScale = 1f;
        cam.transform.position = new Vector3(127, 0, -1);
        //player.SetActive(true);
        sectionTrigger.transform.position = new Vector2(179, 0);
        OnStartThirdSection?.Invoke();
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void EndScreenFinal()
    {
        endScreenMeme.anchoredPosition = Vector3.zero;
        Time.timeScale = 0;

    }

    public void EndScreenDeath()
    {
        endScreen.anchoredPosition = Vector3.zero;
        Time.timeScale = 0;
    }

    public void Pause()
    {
        Time.timeScale = 0f;
        
        
    }

    

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        cam = FindObjectOfType<CameraScript>().gameObject;
        player = FindObjectOfType<PlayerController>().gameObject;
        sectionTrigger = FindObjectOfType<SectionsTrigger>().gameObject;

        endScreen = FindObjectOfType<EndScreenDeath>().gameObject.GetComponent<RectTransform>();
        endScreenMeme = FindObjectOfType<EndScreenMeme>().gameObject.GetComponent<RectTransform>();
        endScreen.anchoredPosition = Vector2.up * 1000;
        endScreenMeme.anchoredPosition = Vector2.up * 1000;
        ThirdSectionCounter = 0;
        Time.timeScale = 0;

        
        
    }

}
