
using System.Collections;


using UnityEngine;


public class EnemySpawner : MonoBehaviour
{
    
    [SerializeField] GameObject enemyType1;
    [SerializeField] GameObject enemyType2;
    [SerializeField] GameObject enemyType3;
    [SerializeField] GameObject boss;
    

    private void OnEnable()
    {
        SectionsTrigger.OnFirstSection += SpawnBaseEnemy;
        SectionsTrigger.OnSecondSection += SpawnMidEnemy;
        SectionsTrigger.OnThirdSection += SpawnTank; 
    }

    private void OnDisable()
    {
        SectionsTrigger.OnFirstSection -= SpawnBaseEnemy;
        SectionsTrigger.OnSecondSection -= SpawnMidEnemy;
        SectionsTrigger.OnThirdSection -= SpawnTank;

    }
   

    

    void SpawnBaseEnemy()
    {
        StartCoroutine(SpawnEnemeyType1());
    }

    IEnumerator SpawnEnemeyType1()
    {
        
        int enemyToSpawn = 30;

       for (int i = 0; i< enemyToSpawn; i++)
        {
            transform.position = new Vector2(transform.position.x, Random.Range(-7,8));
            Instantiate(enemyType1, transform.position, enemyType1.transform.rotation);
            yield return new WaitForSeconds(1);
        }
       yield return null;

    }

    void SpawnMidEnemy()
    { 
        StartCoroutine (SpawnEnemeyType2());
    }

    IEnumerator SpawnEnemeyType2()
    {
        int enemyToSpawn = 20;

        for (int i = 0; i < enemyToSpawn; i++)
        {
            transform.position = new Vector2(transform.position.x, Random.Range(-7, 7));
            Instantiate(enemyType2, transform.position, enemyType2.transform.rotation);
            yield return new WaitForSeconds(2);
        }
        yield return null;
    }

    void SpawnTank()
    {
        StartCoroutine(SpawnEenemyType3());
    }

    IEnumerator SpawnEenemyType3()
    {
        int enemyToSpawn = 5;

        for (int i = 0; i < enemyToSpawn; i++)
        {
            transform.position = new Vector2(transform.position.x, Random.Range(-3, 4));
            Instantiate(enemyType3, transform.position , enemyType3.transform.rotation);
            yield return new WaitForSeconds(10);
        }
        yield return null;
    }


        


}
